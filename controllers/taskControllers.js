// import the Task model in our controllers. So that  our controllers function have access to our Task Model (method, schema)
const Task = require("../models/Task");

// Syntax app.httpMethods("/enpoint", (anonymousFunc));

// Create task: create a task without duplicate names.

module.exports.createTaskControllers = (req, res) => {
	// checking captured data from the request body.
	console.log(req.body);

	Task.findOne({name: req.body.name}).then(result =>{
		console.log(result);

		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found")
		}
		else{
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	})
	.catch(error => res.send(error))
};

// console.log("This is controllers");

// Retrieve All Task 
module.exports.getAllTaskController = (req, res) => {
	Task.find({})
	.then(tasks => res.send(tasks))
	.catch(error => res.send(error));
}

// Retrieve a single task 
module.exports.getSingleTaskCOntroller = (req, res) =>{
	console.log(req.params);

	Task.findById(req.params.taskId)
	.then(result => res.send(result))
	.catch(error => res.send(error));
}

// Updating task status
module.exports.updateTaskStatusController = (req, res) => {
	console.log(req.params.taskId);
	console.log(req.body);

	let updates = {
		status: req.body.status
	};

	// Syntax: findByIdAndUpdate(_id, {objectUpdate}, options)
		// {new: true} - returns the updated version of the document
	Task.findByIdAndUpdate(req.params.taskId, updates, {new: true})
	.then(updatedTask => res.send(updatedask))
	.catch(error => res.send(error));
}

// deleting a Routes
module.exports.deleteTaskController = (req, res) => {
	console.log(req.params.taskId);

	Task.findByIdAndRemove(req.params.taskId)
	.then(removedTask => res.send(removedTask))
	.catch(error => res.send(error));
}