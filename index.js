// require the installed modules
const express = require("express");
const mongoose = require("mongoose");

// port
const port = 4000;

// server
const app = express();

// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@cluster0.mffql.mongodb.net/b203_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Routes Grouping - organize the access for each resources.
const taskRoutes = require("./routes/taskRoutes");
app.use("/tasks", taskRoutes); //localhost:4000/tasks/

// port listener
app.listen(port, () => console.log(`Server is runnig at port ${port}`))

/*

	Separation of Concerns
	Models Folder
	- Contain the Object Schemas and defines the object structure and content

	Controllers Folder
	- Contain the functions and business logic of our Express JS application
	- Meaning all the operations it can do will be placed in this file

	Routes Folder
	- Contains all the endpoints for our application
	- We separate the routes such that "index.js" only contains information on the server
	
	require (import) -> to include a specific module/package.
	export -> to treat a value as a "module" that can be used by other files

	Flow of exports and require: 
	export models > require in controllers 
	export controllers > require in routes
	export routes > require in index.js

	Without separating the Model, Controller, and Routes
		// Inside the server:
		const schemaName = new mongoose.Schema({
			property : {
				setting: value
			}
		})

		const ModelName = mongoose.model("CollectionName", schemaName);

		app.httpMethods("/endpoint", (req, res) => {
			// Business logic (Code Block)
		});

	Separating the Model, Controller, and Routes

		//Inside the Model Folder (Each model is separated by its own .js file)
		const schemaName = new mongoose.Schema({
			property : {
				setting: value
			}
		})
		
		//We export the model so we could access the schema and model in our controllers.
		module.exports = mongoose.model("CollectionName", schemaName);
		
		======================================================================================

		//Inside the countrollers folder (Each controllers is separated by its own .js file)
		// We import (require) the model in the controllers folder so we can access the mongoose schema and methods
		const ModelName  = require("../model/modelFileName");
		
		// We export each function so it will be accessible in our routers and it will run the specific business logic/function upon client request.
		module.exports.FunctionName = (req, res) =>{
			// Business logic (Code Block)
		}

		======================================================================================

		//Inside the routes folder (Each routes is separated by its own .js file)
		const express = require("express");
		// express.Router() method allows access to HTTP methods.
		const router = express.Router();
		
		// We import the controller in the routes file so we can run the specific function upon a client request is sent.
		const controllerName = 	require("../controllers/controllerFileName");
		
		//The anonymous function inside the controller will run upon the client request.
		router.httpMethod("/endpoint", controllerName.functionName);
		
		======================================================================================

		//Inside the index.js (server)
		// We import the routes file in our server so we will be able to received a request on an endpoint with a specified http methods.
		const routeName = require("./routes/routeFileName");
		
		// We usually group the routes based on its collection name, so we can easily identify which collection will be used in the server response.
		app.use("api/collectionName", routeName); 
		
		URL Structure example:
		//localhost:4000/collectionName/

		- "localhost:4000" => current port where we listen to the request
		- "/collectionName" => denotes the resourses that we will access
		- "/" => determines the endpoint where we are going to send a response based on the set http methods.

*/